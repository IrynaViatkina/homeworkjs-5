//проверка
/*const user = {
	firstName: 'Ivan',
	lastName: 'Ivanov',
	contacts: {
		phone: "380445535535",
		mail: {
			gmail: 'gmail.com',
			ukr: 'ukr.net'
		}
	},
	parameters: [25, 170, 65],
	address: {
		country: 'Ukraine',
		city: 'Kiev',
		street: 'Buzkova'
	}
}

const cloneOb = cloneObject(user);*/

function cloneObject(object){
	const clone = {};
	for(let key in object){
		if(Array.isArray(object[key])){
			clone[key] = object[key].slice();
		}
		else if (typeof (object[key]) == "object" && object[key] != null) {
			clone[key] = cloneObject(object[key]);
		}
		else{
			clone[key] = object[key];
		}
	}
	return clone;
}